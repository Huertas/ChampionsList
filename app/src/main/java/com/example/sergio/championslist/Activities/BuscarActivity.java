package com.example.sergio.championslist.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sergio.championslist.ChampionInfo.LolApiConnection;
import com.example.sergio.championslist.Data.User;
import com.example.sergio.championslist.R;

public class BuscarActivity extends AppCompatActivity {


    TextView queue, tier, division, lp, wins, loses;
    ImageView imagen;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.busqueda);

        EditText cuadroBusqueda = (EditText) findViewById(R.id.editText);
        cuadroBusqueda.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                new LolApiConnection.GetUserTask((u)->mostrarUsuario(u)).execute(v.getText().toString());
                return true;
            }
            return false;
        });

        queue = (TextView) findViewById(R.id.queue);
        tier = (TextView) findViewById(R.id.tier);
        division = (TextView) findViewById(R.id.division);
        lp = (TextView) findViewById(R.id.lp);
        wins = (TextView) findViewById(R.id.wins);
        loses = (TextView) findViewById(R.id.losses);
        imagen = (ImageView) findViewById(R.id.imageView);

    }

    private void mostrarUsuario(User u){
        if(u==null){
            //TODO rellenar datos de no se ha encontrado
            return;
        }
        switch(u.getTier()){
            case "BRONZE":
                imagen.setImageResource(R.drawable.bronze);
                break;
            case "SILVER":
                imagen.setImageResource(R.drawable.silver);
                break;
            case "GOLD":
                imagen.setImageResource(R.drawable.gold);
                break;
            case "PLATINUM":
                imagen.setImageResource(R.drawable.platinum);
                break;
            case "DIAMOND":
                imagen.setImageResource(R.drawable.diamond);
                break;
            default:
                imagen.setImageResource(R.drawable.provisional);
                tier.setText("Unranked");
                queue.setText("");
                division.setText("");
                wins.setText("");
                loses.setText("");
                lp.setText("");

                return;

        }
        queue.setText(u.getQueue());
        tier.setText(u.getTier());
        division.setText(u.getDivision());
        wins.setText(Integer.toString(u.getWins()));
        loses.setText(Integer.toString(u.getLosses()));
        lp.setText(Integer.toString(u.getLp()));

    }


}
