package com.example.sergio.championslist.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sergio.championslist.ChampionInfo.LolApiConnection;
import com.example.sergio.championslist.Data.Champion;
import com.example.sergio.championslist.Data.ImageHelper.ImageLoader;
import com.example.sergio.championslist.R;
import com.example.sergio.championslist.Data.ImageDownloaderTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

import static com.example.sergio.championslist.ChampionInfo.LolApiConnection.champList;

public class CategoriesActivity extends AppCompatActivity {

    Champion[] champListCategory = {};
    static final String[] TAGS = {"All","Fighter","Tank","Mage","Support","Assassin","Marksman"};
    static final String[] TAGNames = {"Todos","Guerrero","Tanque","Mago","Soporte","Asesino","Tirador"};
    SelectorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        TabLayout tabs = (TabLayout) findViewById(R.id.tab_layout);
        for(int i = 0;i<TAGS.length;i++){
            tabs.addTab(tabs.newTab().setText(TAGNames[i]).setTag(TAGS[i]));
        }
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabs.setOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        changeTab(tab);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                }
        );

        GridView gridview = (GridView)
                findViewById(R.id.gridview);
        adapter = new SelectorAdapter(this);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener((parent, v, position, id) -> {
            Intent i = new Intent(CategoriesActivity.this,ChampionInfoActivity.class);
            i.putExtra("id",id);
            startActivity(i);
        });
        LolApiConnection.retrieveChampList(()->{
            champListCategory = LolApiConnection.champList;
            adapter.notifyDataSetChanged();
        });
    }

    void changeTab(TabLayout.Tab tab){
        String type = (String) tab.getTag();
        if (type.equals("All")){
            champListCategory = LolApiConnection.champList;
            adapter.notifyDataSetChanged();
            return;
        }
        Champion [] tempChampListCategory = new Champion[LolApiConnection.champList.length];
        int i = 0;
        for(Champion champion : LolApiConnection.champList){
            if(champion.hasTag(type)){
                tempChampListCategory[i++]=champion;
            }
        }
        champListCategory = new Champion[i];
        for(int j =0;j<i;j++){
            champListCategory[j]=tempChampListCategory[j];
        }
        java.util.Arrays.sort(champList);
        adapter.notifyDataSetChanged();
    }

    private class SelectorAdapter extends BaseAdapter {

        LayoutInflater inflater;
        ImageLoader imageLoader;

        public SelectorAdapter(CategoriesActivity championListActivity) {
            inflater = (LayoutInflater)championListActivity.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            imageLoader = LolApiConnection.getImageLoader();
        }


        @Override
        public int getCount() {
            return champListCategory.length;
        }

        @Override
        public Object getItem(int i) {
            return champListCategory[i];
        }

        @Override
        public long getItemId(int i) {
            return champListCategory[i].getId();
        }

        @Override
        public android.view.View getView(int i, View view, ViewGroup viewGroup) {
            view = inflater.inflate(R.layout.grid_element,null);
            ImageView champImage = (ImageView) view.findViewById(R.id.champImage);
            imageLoader.DisplayImage(champListCategory[i].getSquareImageURL(),champImage);
            TextView champName = (TextView) view.findViewById(R.id.champName);
            champName.setText(champListCategory[i].getName());
            TextView champTitle = (TextView) view.findViewById(R.id.champTitle);
            champTitle.setText(champListCategory[i].getTitle());
            return view;
        }
    }
}