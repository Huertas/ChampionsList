package com.example.sergio.championslist.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sergio.championslist.Data.ImageHelper.ImageLoader;
import com.example.sergio.championslist.R;
import com.example.sergio.championslist.Data.Champion;
import com.example.sergio.championslist.ChampionInfo.LolApiConnection;

public class ChampionInfoActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long id = getIntent().getLongExtra("id",-1);
        if(id<0){
            finish();
            return;
        }
        setContentView(R.layout.activity_champion_info);
        new LolApiConnection.GetChampTask(champion->onChampionGotten(champion)).execute(id);
    }

    public void onChampionGotten(Champion champion){
        findViewById(R.id.progressBar).setVisibility(View.GONE);
        findViewById(R.id.champInfoContainer).setVisibility(View.VISIBLE);
        findViewById(R.id.champScrollContainer).setVisibility(View.VISIBLE);

        ImageLoader imageLoader = LolApiConnection.getImageLoader();

        ((TextView) findViewById(R.id.champName)).setText(champion.getName());
        ((TextView) findViewById(R.id.champTitle)).setText(champion.getTitle());
        imageLoader.DisplayImage(champion.getSquareImageURL(),(ImageView) findViewById(R.id.champImage));


        ((TextView) findViewById(R.id.passiveName)).setText(champion.getPassive().getName());
        ((TextView) findViewById(R.id.passiveDescription)).setText(champion.getPassive().getDescription());
        imageLoader.DisplayImage(champion.getPassive().getImageURL(),(ImageView) findViewById(R.id.passiveImage));

        ((TextView) findViewById(R.id.QName)).setText(champion.getSpell(0).getName());
        ((TextView) findViewById(R.id.QDescription)).setText(champion.getSpell(0).getDescription());
        imageLoader.DisplayImage(champion.getSpell(0).getImageURL(),(ImageView) findViewById(R.id.QImage));

        ((TextView) findViewById(R.id.WName)).setText(champion.getSpell(1).getName());
        ((TextView) findViewById(R.id.WDescription)).setText(champion.getSpell(1).getDescription());
        imageLoader.DisplayImage(champion.getSpell(1).getImageURL(),(ImageView) findViewById(R.id.WImage));

        ((TextView) findViewById(R.id.EName)).setText(champion.getSpell(2).getName());
        ((TextView) findViewById(R.id.EDescription)).setText(champion.getSpell(2).getDescription());
        imageLoader.DisplayImage(champion.getSpell(2).getImageURL(),(ImageView) findViewById(R.id.EImage));

        ((TextView) findViewById(R.id.RName)).setText(champion.getSpell(3).getName());
        ((TextView) findViewById(R.id.RDescription)).setText(champion.getSpell(3).getDescription());
        imageLoader.DisplayImage(champion.getSpell(3).getImageURL(),(ImageView) findViewById(R.id.RImage));



    }
}
