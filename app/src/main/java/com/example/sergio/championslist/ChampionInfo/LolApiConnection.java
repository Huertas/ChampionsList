package com.example.sergio.championslist.ChampionInfo;

import android.os.AsyncTask;
import android.util.Log;


import com.example.sergio.championslist.Data.Champion;
import com.example.sergio.championslist.Data.ImageHelper.ImageLoader;
import com.example.sergio.championslist.Data.Passive;
import com.example.sergio.championslist.Data.Spell;
import com.example.sergio.championslist.Data.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

public class LolApiConnection {
    private static final String API_KEY = "RGAPI-5191697a-f36e-4f09-b6b0-10399505730a";

    public static Champion[] champList = {};
    public static String version;

    public static void retrieveChampList(){
        new RetrieveChamplistTask(()->{}).execute();
    }
    public static void retrieveChampList(Runnable r){
        new RetrieveChamplistTask(r).execute();
    }

    private static ImageLoader imageLoader;


    public static ImageLoader getImageLoader(){
        if (imageLoader==null)
            imageLoader = new ImageLoader();
        return imageLoader;
    }



    private static class RetrieveChamplistTask extends AsyncTask<Void, Void, Champion[]> {

        Runnable runnable;

        private RetrieveChamplistTask(Runnable r) {
            runnable = r;
        }

        @Override
        protected Champion[] doInBackground(Void... voids) {
            Champion[] champListTemp = null;
            String s = GET("https://euw1.api.riotgames.com/lol/static-data/v3/champions?champListData=image,tags&api_key=" + API_KEY);
            if (s == null)
                return null;
            try {
                JSONObject champInfo = new JSONObject(s);
                version = champInfo.getString("version");
                JSONObject data = champInfo.getJSONObject("data");
                champListTemp = new Champion[data.length()];
                Iterator<String> it = data.keys();
                for (int i = 0; i < data.length(); i++) {
                    JSONObject JSONchampion = data.getJSONObject(it.next());
                    champListTemp[i] = new Champion(JSONchampion.getInt("id"));
                    champListTemp[i].setName(JSONchampion.getString("name"));
                    champListTemp[i].setTitle(JSONchampion.getString("title"));
                    champListTemp[i].setKey(JSONchampion.getString("key"));
                    champListTemp[i].setSquareImage(JSONchampion.getJSONObject("image").getString("full"));
                    JSONArray JSONTags = JSONchampion.getJSONArray("tags");
                    String[] tagList = new String[JSONTags.length()];
                    for (int x = 0; x < JSONTags.length(); x++) {
                        tagList[x] = JSONTags.getString(x);
                    }
                    champListTemp[i].setTags(tagList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return champListTemp;
        }

        @Override
        protected void onPostExecute(Champion[] result) {
            champList = result;
            java.util.Arrays.sort(champList);
            runnable.run();
        }
    }

    public static class GetUserTask extends AsyncTask<String, Void, User>{

        Consumer<User> consumer;

        public GetUserTask(Consumer<User> consumer){
            this.consumer = consumer;
        }

        @Override
        protected User doInBackground(String... strings) {
            int id;
            String URL="https://euw1.api.riotgames.com/lol/summoner/v3/summoners/by-name/"+strings[0]+"?api_key="+API_KEY;
            String result = GET(URL);
            try{
                JSONObject lookingForIDPlayer = new JSONObject(result);
                id=lookingForIDPlayer.getInt("id");
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
            User searched=null;
            String playerResult = GET("https://euw.api.riotgames.com/api/lol/EUW/v2.5/league/by-summoner/"+ id +"/entry?api_key="+API_KEY);
            if(playerResult==null)
                return null;
            try{

                JSONObject userInfo = new JSONObject(playerResult);
                JSONArray arrayLigas = userInfo.getJSONArray(Integer.toString(id));
                if(arrayLigas.length()==0){
                    searched = new User(id);
                }else{
                    JSONObject info = arrayLigas.getJSONObject(0);
                    searched = new User(id);
                    searched.setTier(info.getString("tier"));
                    searched.setQueue(info.getString("queue"));
                    JSONObject entries = info.getJSONArray("entries").getJSONObject(0);
                    searched.setDivision(entries.getString("division"));
                    searched.setWins(entries.getInt("wins"));
                    searched.setLosses(entries.getInt("losses"));
                    searched.setLp(entries.getInt("leaguePoints"));
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            return searched;
        }

        @Override
        protected void onPostExecute(User result) {
            consumer.accept(result);
        }
    }

    private static String GET(String url){
        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;
        String result = "";
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();
            Log.d("URLRequested",url);

            Log.d("status",""+statusCode);
            if (statusCode != HttpURLConnection.HTTP_OK) {
                return null;
            }

            inputStream = urlConnection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            while((line = bufferedReader.readLine()) != null) {
                result += line;
            }
            inputStream.close();

        } catch (Exception e) {
            urlConnection.disconnect();
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return result;
    }

    public interface Consumer<T>{
        void accept(T t);
    }

    public static class GetChampTask extends AsyncTask<Long, Void, Champion>{

        Consumer<Champion> consumer;

        public GetChampTask(Consumer<Champion> consumer){
            this.consumer = consumer;
        }

        @Override
        protected Champion doInBackground(Long... longs) {
            Champion champTemp = null;
            String s = GET("https://euw1.api.riotgames.com/lol/static-data/v3/champions/" + longs[0].toString() + "?champData=all&api_key="+API_KEY+"&version="+version);
            if(s==null)
                return null;
            try{
                JSONObject champInfo = new JSONObject(s);


                champTemp = new Champion(champInfo.getInt("id"));
                champTemp.setName(champInfo.getString("name"));
                champTemp.setTitle(champInfo.getString("title"));
                champTemp.setKey(champInfo.getString("key"));
                champTemp.setSquareImage(champInfo.getJSONObject("image").getString("full"));
                Passive passive = new Passive();
                JSONObject JSONPassive = champInfo.getJSONObject("passive");
                passive.setName(JSONPassive.getString("name"));
                passive.setDescription(JSONPassive.getString("description"));
                passive.setImage(JSONPassive.getJSONObject("image").getString("full"));
                JSONArray JSONSpells = champInfo.getJSONArray("spells");
                champTemp.setPassive(passive);
                for (int i = 0;i<JSONSpells.length();i++){
                    Spell spell = new Spell();
                    JSONObject JSONSpell = JSONSpells.getJSONObject(i);
                    spell.setName(JSONSpell.getString("name"));
                    spell.setDescription(JSONSpell.getString("description"));
                    spell.setCost(JSONSpell.has("resource")?JSONSpell.getString("resource"):"");
                    spell.setImage(JSONSpell.getJSONObject("image").getString("full"));
                    champTemp.setSpell(i,spell);
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            return champTemp;
        }

        @Override
        protected void onPostExecute(Champion result) {
            consumer.accept(result);
        }
    }
}
