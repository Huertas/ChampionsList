package com.example.sergio.championslist.Data;
import com.example.sergio.championslist.ChampionInfo.LolApiConnection;

public class Champion implements Comparable<Champion>{

    int id;
    String title;
    String key;
    String name;

    String squareImage;

    Spell[] spellList;

    String[] tags;

    Passive passive;


    public Champion(int id){
        this.id = id;
        spellList = new Spell[4];
        tags=new String[2];
    }

    public void setTags(java.lang.String[] tags) {
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSquareImage() {
        return squareImage;
    }

    public void setSquareImage(String squareImage) {
        this.squareImage = squareImage;
    }

    public void setSpellsLength(int i){
        spellList = new Spell[i];
    }

    public Spell getSpell(int i){
        return spellList[i];
    }

    public void setSpell(int i, Spell spell){
        spellList[i] = spell;
    }

    public Passive getPassive() {
        return passive;
    }

    public void setPassive(Passive passive) {
        this.passive = passive;
    }

    public String getSquareImageURL(){
        return "http://ddragon.leagueoflegends.com/cdn/" + LolApiConnection.version + "/img/champion/"+squareImage;
    }

    public String[] getTags() {
        return tags;
    }

    public boolean hasTag(String tag){
        for (String s:tags) {
            if(s.equals(tag)){
                return true;
            }
        }
        return false;
    }

    @Override
    public int compareTo(Champion champion) {
        return name.compareTo(champion.name);
    }
}
