package com.example.sergio.championslist.Data;
import com.example.sergio.championslist.ChampionInfo.LolApiConnection;

public class Passive {
    String image;
    String description;
    String name;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageURL(){
        return "http://ddragon.leagueoflegends.com/cdn/" + LolApiConnection.version + "/img/passive/"+image;
    }
}
