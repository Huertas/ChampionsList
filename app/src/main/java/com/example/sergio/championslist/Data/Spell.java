package com.example.sergio.championslist.Data;

import com.example.sergio.championslist.ChampionInfo.LolApiConnection;

public class Spell {
    String name;
    String description;
    String cost;
    String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageURL(){
        return "http://ddragon.leagueoflegends.com/cdn/" + LolApiConnection.version + "/img/spell/"+image;
    }
}
